CC=gcc
CFLAGS=-Wall -O3
ALL=fifoexec seqchr

all: ${ALL}

fifoexec: fifoexec.c
	${CC} -o $@ ${CFLAGS} $@.c

seqchr: seqchr.c
	${CC} -o $@ ${CFLAGS} $@.c

clean:
	rm -f ${ALL}
