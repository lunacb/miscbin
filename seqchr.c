#include <stdio.h>

void addc(char c)
{
	static int first = 1;

	if(first)
		first = 0;
	else
		putchar(' ');

	putchar(c);
}

int main(int argc, char **argv)
{
	int i, j, k;

	for(i = 1; i < argc; i++) {
		int c = -1;
		int lastc = -1;
		char *str = argv[i];
		int is_range = 0;
		for(j = 1; c != '\0'; j++) {
			c = str[j];
			if(!is_range)
				lastc = str[j-1];

			if(c == '-') {
				is_range = 1;
				continue;
			}
				
			if(is_range) {
				if(c == '\0') {
					addc(lastc);
					addc('-');
					continue;
				}

				is_range = 0;
				for(k = lastc; k < c; k++)
					addc(k);
			} else {
				addc(lastc);
			}


		}
	}

	putchar('\n');

	return 0;
}
