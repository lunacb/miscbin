/*
 * pipe fifo into a command, reopening it if eof is reached
 */

#include <sys/wait.h>
#include <sys/stat.h>
#include <stdio.h>
#include <stdlib.h>
#include <signal.h>
#include <fcntl.h>
#include <stdarg.h>
#include <unistd.h>
#include <errno.h>
#include <string.h>

pid_t fpid = -1;
int pipefd[2] = {0, 0};
int exec_failed = 0;

void help(char *name)
{
	printf("usage: %s fifo command [arguments...]\n", name);
	exit(1);
}

void cleanup(void)
{
	close(pipefd[1]);
	if(fpid != -1) {
		kill(fpid, SIGTERM);
		waitpid(fpid, NULL, 0);
	}
}

void sig_handler(int sig)
{
	cleanup();
	if(exec_failed)
		exit(1);
	else
		exit(0);
}

int handle_signals(int sig1, ...) 
{
	int tmp_sig;
	va_list ap;

	va_start(ap, sig1);

	tmp_sig = sig1;

	do {
		struct sigaction sa;
		memset(&sa, 0, sizeof(struct sigaction));
		sa.sa_handler = sig_handler;
		if(sigaction(tmp_sig, &sa, NULL) == -1)
			return 1;
		tmp_sig = va_arg(ap, int);
	} while(tmp_sig);

	va_end(ap);

	return 0;
}

int main(int argc, char **argv)
{
	char *args[argc-1];
	struct stat statbuf;

	if(argc < 3)
		help(*argv);
	
	if(stat(argv[1], &statbuf) == -1) {
		fprintf(stderr, "%s: failed to stat file %s: %s\n",
			*argv, argv[1], strerror(errno));
		return 1;
	}

	if(!S_ISFIFO(statbuf.st_mode)) {
		fprintf(stderr, "%s: file %s is not a fifo\n",
			*argv, argv[1]);
		help(*argv);
	}

	if(handle_signals(SIGCHLD, SIGINT, SIGTERM, 0)) {
		fprintf(stderr, "%s: could not handle signals: %s\n",
			*argv, strerror(errno));
		return 1;
	}

	memcpy(args, argv + 2, (argc - 2) * sizeof(char *));
	args[argc-2] = NULL;

	if(pipe(pipefd) == -1) {
		fprintf(stderr, "%s: could not create pipe: %s\n",
			*argv, strerror(errno));
		return 1;
	}

	fpid = fork();

	if(fpid == -1) {
		fprintf(stderr, "%s: could not fork: %s\n",
			*argv, strerror(errno));
		return 1;
	} else if (fpid == 0) {
		close(pipefd[1]);
		dup2(pipefd[0], STDIN_FILENO);
		execvp(args[0], args);
		fprintf(stderr, "%s: could not execvp: %s\n",
			*argv, strerror(errno));
		exec_failed = 1;
		return 1;
	}

	close(pipefd[0]);

	for(;;) {
		int fd;
		char buf[512];
		size_t count = -1;

		if((fd = open(argv[1], O_RDONLY | O_CLOEXEC)) == -1) {
			fprintf(stderr, "%s: could not (re)open file %s: %s\n" ,
				*argv, argv[1], strerror(errno));
			cleanup();
			return 1;
		}

		while(count != 0) {
			count = read(fd, buf, sizeof(buf));

			if(count < 0) {
				if(errno != EINTR) {
					fprintf(stderr, "%s: failed to read from"
						" file %s: %s\n",
						*argv, argv[1], strerror(errno));
					cleanup();
					return 1;
				}
			} else if(count > 0) {
				size_t wcount;
				char *buf_ptr = buf;
				while(count) {
					wcount = write(pipefd[1], buf_ptr, count);
					if(wcount == -1) {
						fprintf(stderr, "%s: failed"
							" to write to"
							" file %s: %s\n",
							*argv, argv[1], 
							strerror(errno));
						cleanup();
						return 1;
					}
					count -= wcount;
					buf += wcount;
				}
			}
		}

		close(fd);
	}

	cleanup();
}
